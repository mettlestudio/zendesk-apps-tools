require 'sinatra/base'
require 'sassc'
require 'sinatra/cross_origin'
require 'zendesk_apps_tools/theming/common'

module ZendeskAppsTools
  module Theming
    module Zass
      include Common

      # Compiles the SCSS into CSS.
      #
      # scss - A String containing SCSS.
      #
      # Returns a new CSS String.
      def compile_zass()
        styles_dir = theme_package_path("styles")
        styles_path = File.join(styles_dir, "index.scss")
        styles = File.read(styles_path)

        ignored_variables = manifest["settings"].flat_map { |setting_group| setting_group["variables"] }.map { |variable| variable["identifier"] }
        asset_file_names = Dir.glob(theme_package_path('assets', '*'))
        asset_variables = asset_file_names.map { |asset| File.join('assets', File.basename(asset)).gsub(/[^a-z0-9\-_]+/, '-') }
        ignored_variables.concat(asset_variables)
        preamble = ignored_variables.map {|name| escape_variable(name) }.join("\n")

        # this comes in as a single string
        stylesheet = "#{preamble}\n#{styles}"
        options = {
          syntax: :scss,
          style: :expanded,
          load_paths: [styles_dir],
          filename: "style.scss",
        }

        SassC::Engine.new(stylesheet, options).render.tap do |css|
          unescape_variables!(css)
        end
      end

      private

      # Escapes that variable by generating a Sass variable assignment.
      #
      # Returns a String Sass variable assignment.
      def escape_variable(name)
        "$#{name}: \\$#{name};"
      end

      # Unescapes an escaped variable.
      #
      # Returns a String.
      def unescape_variables!(css)
        css.gsub!("\\$", "$")
      end
    end
  end
end
