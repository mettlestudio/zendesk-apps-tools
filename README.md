# Zendesk Apps Tools

## Description

A fork from Zendesk Apps Tools (ZAT) with some bug fixes and support for SASS.

## Owners

This repo is owned and maintained by the Mettle Studio.

It is a fork of the [Zendesk Apps Tools](https://github.com/zendesk/zendesk_apps_tools) from the Zendesk Apps team.

## How to use

Clone this repo and run `bundle install`.

Then simply run from your custom theme directory using `path/to/this/directory/bin/zat theme preview`

## Contribute

Improvements are always welcome. To contribute:

- Put up a PR into the main branch.
- Add Sam Parkinson as the reviewer.

## Bugs

You can report bugs as issues here on BitBucket.
